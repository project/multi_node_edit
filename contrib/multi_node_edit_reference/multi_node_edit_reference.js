$(document).ready(function() {
  var speed = 'slow';
  
  $('select.multi-node-edit-reorder-dropdown').change(function (){    
    var select = this;
    // parent() nightmare can be avoided with closest() in Jquery 1.3
    var this_fieldset = $(this).parent().parent().parent().parent().parent();    
    var this_order = $(this_fieldset).attr('class').match(/order-\d+/g);
    this_order = this_order.toString().substring(6);

    // Move the fieldset
    $(this_fieldset).hide(speed);
    if($(this).val() < this_order) {
      $(this_fieldset).insertBefore('fieldset.multi-node-edit-order-' + $(this).val());
    } 
    else {
      $(this_fieldset).insertAfter('fieldset.multi-node-edit-order-' + $(this).val());
    }
    $.scrollTo('fieldset.multi-node-edit-order-' + this_order, speed);
    $(this_fieldset).show(speed);
    
    multi_node_edit_reorder();
    
  });
  
  $('input.multi-node-edit-remove-checkbox').change(function() {
    // parent() nightmare can be avoided with closest() in Jquery 1.3
    var this_fieldset = $(this).parent().parent().parent().parent().parent().parent();
    this_fieldset.hide(speed);
    this_fieldset.attr('class', '');
    
    multi_node_edit_reorder();
  });
});

function multi_node_edit_reorder(){
  // Remove un-needed select elements
  var total = $('fieldset.reorderable').size();
  $('select.multi-node-edit-reorder-dropdown option').each( function() { 
    if ($(this).val() > total) {
      $(this).remove();
    }
  });

  // Update to reflect the new order
  var count = 1;
  $('fieldset.reorderable').each(function() {
    // Update the ID
    $(this).attr('class', $(this).attr('class').replace(/order-\d+/g, 'order-' + count));
  
    // Update the select default
    $(this).find('select.multi-node-edit-reorder-dropdown option[selected]').removeAttr('selected');
    $(this).find('select.multi-node-edit-reorder-dropdown option[value=' + count + ']').attr('selected', "selected");
        
    count++;
  });
}
